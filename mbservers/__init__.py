from .mbservers import MBServersCog

def setup(bot):
    bot.add_cog(MBServersCog(bot))

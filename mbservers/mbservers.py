from redbot.core import commands
import discord
from bs4 import BeautifulSoup
import urllib.request
import time, datetime
import pytz

class MBServersCog(commands.Cog):
    """Display Mount&Blade server list"""
    
    def __init__(self, bot):
        self.bot = bot
        self.last_update = None
    
    def get_server_list(self):
        html = urllib.request.urlopen('http://www.mnbcentral.net/').read()
        soup = BeautifulSoup(html, 'html.parser')
        
        servers = []
        for row in soup.tbody.find_all('tr'):
            server = []
            for col in row.find_all('td'):
                server.append(col.get_text())
            servers.append(server)
        
        # sorting by current player count
        servers = sorted(servers, key=lambda x: int(x[5]), reverse=True)
        
        last_update_string = soup.table.next_sibling.strip()
        self.last_update = datetime.datetime(
            *time.strptime(last_update_string, 'Laste Updated on %m/%d/%y %H:%M:%S')[:6],
            tzinfo=pytz.timezone('EST')
        )
        
        return servers
    
    def search_column(self, servers, column, value):
        return filter(lambda x: value.lower() in x[column].lower(), servers)
    
    async def print_servers(self, ctx, servers, title="Servers"):
        if type(servers) != 'list':
            servers = list(servers)
        
        embed = discord.Embed(
            title=title,
            timestamp=self.last_update,
            color=await ctx.embed_color(),
        )
        
        embed.set_footer(text="Checked at")
        
        for server in servers[:25]:
            embed.add_field(name=server[0], value="%s/%s" % (server[5], server[6]))
        
        await ctx.send(embed=embed)
    
    @commands.group()
    async def mbservers(self, ctx):
        """Mount & Blade server list"""
        pass
    
    @mbservers.command()
    async def nwp(self, ctx):
        """Show NWP servers"""
        servers = self.get_server_list()
        
        nwp_servers = [
            'Minisiege',
            'EU_Commander',
            'Mininaval',
            'Caribbean_Republic',
        ]
        
        filtered_servers = filter(lambda x: x[0] in nwp_servers, servers)
        
        await self.print_servers(ctx, filtered_servers, title="NWP Servers")
    
    @mbservers.command()
    async def all(self, ctx):
        """Show all servers"""
        servers = self.get_server_list()
        
        await self.print_servers(ctx, servers)
    
    @mbservers.command()
    async def name(self, ctx, search):
        """Filter servers by name"""
        servers = self.get_server_list()
        filtered_servers = self.search_column(servers, 0, search)
        
        await self.print_servers(ctx, filtered_servers)
    
    @mbservers.command()
    async def module(self, ctx, search):
        """Filter servers by module"""
        servers = self.get_server_list()
        filtered_servers = self.search_column(servers, 1, search)
        
        await self.print_servers(ctx, filtered_servers)